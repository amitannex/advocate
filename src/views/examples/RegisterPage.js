// reactstrap components
import { Button, Card, Form, Input, Container, Row, Col } from "reactstrap";
import {Link } from 'react-router-dom';
// core components
import IndexNavbar from "components/Navbars/IndexNavbar";
import React from 'react';

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fname:"",
      lname:"",
      email:"",
      phone:"",
      regNo:""
    };
    document.documentElement.classList.remove("nav-open");
  }

  componentDidMount() {
    document.body.classList.add("register-page");
    return function cleanup() {
      document.body.classList.remove("register-page");
    };
  }

  handleChange=(e)=>{
    const { name, value } = e.target;
    // console.log("value "+e.target.value);
    this.setState({ [name]: value });
  }

  handleSubmit=(e)=>{
    e.preventDefault();
    this.setState({ submitted: true });
    const { fname, lname,email,phone,regNo } = this.state;
    if (!fname || !lname || !email || !phone || !regNo) {            
      alert("please enter mandatory fields")
    }   
  }

  render() {
    return (
      <div>
      <IndexNavbar />
      <div
        className="page-header"
        style={{
          backgroundImage: "url(" + require("assets/img/login-image.jpg") + ")"
        }}
      >
        <div className="filter" />
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" lg="4">
              <Card className="card-register ml-auto mr-auto">
                <h3 className="title mx-auto">Welcome</h3>
                 <Form className="register-form" onSubmit={this.handleSubmit}>
                  <label>First name</label>
                  <Input placeholder="First name" name="fname" type="text" onChange={this.handleChange} />
                  <label>Last name</label>
                  <Input placeholder="Last name"  name="lname" type="text" onChange={this.handleChange}/>
                  <label>Email</label>
                  <Input placeholder="Email" name="email" type="text" onChange={this.handleChange}/>
                  <label>Mobile</label>
                  <Input placeholder="Mobile" maxLength="10" name="phone" type="number" onChange={this.handleChange}/>
                  <label>Registration number</label>
                  <Input placeholder="Registration number" name="regNo" type="text" onChange={this.handleChange}/>
                  <Button type="submit" block className="btn-round" color="danger">
                    Register
                  </Button>
                <div className="forgot">
                  <Link to="/login" className="btn btn-link">
                    <Button
                      className="btn-link"
                      color="danger"
                    >
                    Already Registered? Login!
                  </Button>
                  </Link>
                </div>
                </Form>
              </Card>
            </Col>
          </Row>
        </Container>
        </div>
        </div>
    );
  }
}

export default RegisterPage;
