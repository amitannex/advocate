import React from "react";
// reactstrap components
import { Button, Card, Form, Input, Container, Row, Col } from "reactstrap";

// core components
import IndexNavbar from "components/Navbars/IndexNavbar";

class ContactUsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name:"",
      email:"",
      ques:""
    };
  }


  componentDidMount() {
    document.body.classList.add("contact-us");
    return function cleanup() {
      document.body.classList.remove("contact-us");
    };    
  }

  handleChange=(e)=>{
    const { name, value } = e.target;
    // console.log("value "+e.target.value);
    this.setState({ [name]: value });

  }

  handleSubmit=(e)=>{
    e.preventDefault();
    this.setState({ submitted: true });
    const { name,email,ques } = this.state;
    if (!name || !email || !ques) {            
      alert("please enter mandatory fields")
  }
}

render() {
  return (
    <>
      <IndexNavbar />
      <div
        className="page-header"
        style={{
          backgroundImage: "url(" + require("assets/img/login-image.jpg") + ")"
        }}
      >
        <div className="filter" />
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" lg="4">
              <Card className="card-register ml-auto mr-auto">
                <h3 className="title mx-auto">Ask us</h3>
                 <Form className="register-form" onSubmit={this.handleSubmit}>
                  <label>Name</label>
                  <Input placeholder="Name" name="fname" type="text" onChange={this.handleChange} />
                  <label>Email</label>
                  <Input placeholder="Email" name="email" type="text" onChange={this.handleChange}/>
                  <label>Question</label>
                  <Input placeholder="Type here...." name="ques" type="text" onChange={this.handleChange}/>
                  <Button type="submit" block className="btn-round" color="danger">
                    Submit
                  </Button>
                </Form>
              </Card>
            </Col>
          </Row>
        </Container>
        </div>
        </>
    );
  }
}

export default ContactUsPage;
