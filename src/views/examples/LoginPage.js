import React from "react";
// reactstrap components
import { Button, Card, Form, Input, Container, Row, Col } from "reactstrap";
import { Link } from "react-router-dom";

// core components
import IndexNavbar from "components/Navbars/IndexNavbar";
class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone:""
    };
    document.documentElement.classList.remove("nav-open");

  }
    componentDidMount() {
      document.body.classList.add("login");
      return function cleanup() {
        document.body.classList.remove("login");
      };    
    }

    handleChange=(e)=>{
      const { name, value } = e.target;
      // console.log("value "+e.target.value);
      this.setState({ [name]: value });
  
    }
  
    handleSubmit=(e)=>{
      e.preventDefault();
      this.setState({ submitted: true });
      const { phone } = this.state;
      if (!phone) {
              
        alert("please enter mandatory field")
    }
  }
   
  render() {
    return (
      <div>
      <IndexNavbar />
      <div
        className="page-header"
        style={{
          backgroundImage: "url(" + require("assets/img/login-image.jpg") + ")"
        }}
      >
        <div className="filter" />
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" lg="4">
              <Card className="card-register ml-auto mr-auto">
                <h3 className="title mx-auto">Login</h3>
                 <Form className="register-form" onSubmit={this.handleSubmit}>                  
                 <label>Mobile</label>
                  <Input placeholder="Mobile" name="phone" type="number" onChange={this.handleChange}/>
                  <Button type="submit" block className="btn-round" color="danger">
                    Submit
                  </Button>
               
                <div className="forgot">
                  <Link to="/register" className="btn btn-link">
                    <Button
                      className="btn-link"
                      color="danger"
                    >
                    Not Registered with us? Register here!
                  </Button></Link>                  
                </div>
                </Form>
              </Card>
            </Col>
          </Row>
        </Container>
        </div>
        </div>
      );
    }
  }
export default LoginPage;
